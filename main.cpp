#include <QGuiApplication>

#include <QtQuick/QQuickView>

#include "FboInSGRenderer.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<FboInSGRenderer>("SceneGraphRendering", 1, 0, "Renderer");

    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl("qrc:///scenegraph/textureinsgnode/main.qml"));
    view.show();

    return app.exec();
}
