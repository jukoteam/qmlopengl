#ifndef FBOINSGRENDERER_H
#define FBOINSGRENDERER_H

#include <QtQuick/QQuickFramebufferObject>

class LogoRenderer;

class FboInSGRenderer: public QQuickFramebufferObject
{
    Q_OBJECT
public:
    FboInSGRenderer() {}

    QQuickFramebufferObject::Renderer *createRenderer() const;
};

#endif // FBOINSGRENDERER_H
